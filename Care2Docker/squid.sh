export FRONTIER_USER=dbfrontier
export FRONTIER_GROUP=dbfrontier

# Download and Install frontier-squid
rpm -Uvh http://frontier.cern.ch/dist/rpms/RPMS/noarch/frontier-release-1.1-1.noarch.rpm
yum -y install frontier-squid

# Make persistent configurations.
chkconfig frontier-squid on
cp -f customize.sh /etc/squid/customize.sh
rm -f customize.sh
chmod +x /etc/squid/customize.sh
service frontier-squid start
squidclient mgr:offline_toggle
echo "export SQUID_CLEAN_CACHE_ON_START=false" > /etc/sysconfig/frontier-squid
service frontier-squid cleancache || true

# Update all site-loca-config.xml.
for dir in /opt/cms/*/
do
    find $dir -name "site-local-config.xml" | while read f
    do
        diff=$(diff site-local-config.xml $f)
        if [ -n "$diff" ];
        then
            echo Updating $f 
            cp -f site-local-config.xml $f
        fi
    done
done

# Start squid
service frontier-squid reload || true

# With squid running, run the care script once.
echo Starting to run care script to fill squid cache.
sh re-execute.sh 
echo Finished running care script.

# Change squid to offline mode.
cp -f customize_offline.sh /etc/squid/customize.sh
rm -f customize_offline.sh
service frontier-squid reload || true

# Make squid start with the docker.
awk 'NR==2{print "service frontier-squid start > /dev/null 2>&1"}7' re-execute.sh > re-execute.sh.2
mv -f /re-execute.sh.2 /re-execute.sh
