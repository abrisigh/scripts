import argparse
import os.path
import os
import sys
import subprocess
import pickle

try:
    import FWCore.ParameterSet.Config as cms
except ImportError:
    print "Error: Can find CMS enviroment. Run \"source /run.sh\" or call generate.sh"
    sys.exit()

DIC_PATH = '/das_dic'

# Used to check if CMSRun exists.
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

# Cut a root file and save it in the correct position. Checks if it locally and, if it is, replace it.
def cut_root_file(filename, events):
    print 'Cutting ' + filename + ' to ' + str(events) +  ' events.'    
    fname = "/data/" + filename
    try:
        os.remove("cut.py")
    except OSError:
        pass

    with open("cut.py", "w") as cut:
        # Give the process a name
        cut.write("process = cms.Process(\"PickEvent\")\n")

        # Tell the process which files to use as the sourdce
        cut.write("process.source = cms.Source (\"PoolSource\",\n")
        cut.write("            fileNames = cms.untracked.vstring (\"file:/data/" + filename + "\")\n")
        cut.write(")\n")

        # tell the process to only run over 100 events (-1 would mean run over
        #  everything
        cut.write("process.maxEvents = cms.untracked.PSet(\n")
        cut.write("            input = cms.untracked.int32 ("+ str(events) + ")\n")
        cut.write(")\n")

        # Tell the process what filename to use to save the output
        cut.write("process.Out = cms.OutputModule(\"PoolOutputModule\",\n")
        cut.write("         fileName = cms.untracked.string (\"/data/MyOutputFile.root\")\n")
        cut.write(")\n")

        # make sure everything is hooked up
        cut.write("process.end = cms.EndPath(process.Out)\n")

    cmsRun = subprocess.Popen('cmsRun cut.py', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cmsRun.wait()
    print "Cutting finished, fixing data names."
    if os.path.isfile(fname):
        print 'Local file will be overwritten.'
        p = subprocess.Popen('mv -f /data/MyOutputFile.root ' + fname, shell=True, stdout=subprocess.PIPE)

# return format [ [workflow name, [file list], cut number, [steps], [extra_files] ] * N ]
def parse_creating_log():
    if os.path.isfile('/workflows'):
        print 'Using workflows file'
        return pickle.load(open('/workflows', 'rb'))

    with open('creating.log', 'r') as file:
        lines = file.readlines() 

    table = []
    w_name = ''
    for i in range(len(lines)):
        # start of new workflow.
        if lines[i].startswith("Preparing"):
            # save the other.
            if w_name:
                table.append([ w_name, files, cut, steps ])

            # join name because it may contain spaces, remove \n 
            w_name = ''.join(lines[i].split(' ')[3:])[:-1]     
            steps = []
            files = []
            cut = 0

        # if it does, next line is a step.
        if lines[i].startswith("# in:"):
            new_step = lines[i+1][1:]
            # check for multiple lines steps
            j = i+2
            while (j < len(lines)) and (not lines[j].startswith("[")) and (not lines[j].startswith('# in:')) and bool(lines[j].strip()) and (bool(lines[j])):
                if bool(lines[j].strip()):
                    new_step = new_step + lines[j]
                j = j+1


            steps.append(new_step)

            # cut checking, look for -n option, get next value and update cut to the maximum value.
            if "dasquery.log" in new_step:
                new_step_split = new_step.split(' ')
                cut_il = [ind for ind in range(len(new_step_split)) if "-n" == new_step_split[ind]]
                cut = max([cut] + ([int(new_step_split[j+1]) for j in cut_il]))

        # file list arrives.
        if lines[i].startswith("["):
            if len(lines[i]) > 3:
                # remove [], \n, ' and spaces. Split using ','.
                files = lines[i][1:-2].replace('\'','').replace(' ','').split(',')

        # save the last one
        if lines[i].startswith("Cannot"):
            table.append([ w_name, files, cut, steps, [] ])

    return table

# Try to restore path using /das_dic created by modified das_client.py
def restore_path(filename):
    if not os.path.isfile('/das_dic'):
        print 'Error: ./das_dic not found. Remove creating log and run ./generate.sh again.'
        sys.exit()

    dic = pickle.load(open(DIC_PATH,"rb"))
    fname = filename.split("/")[-1]

    for k in dic:
        for l in dic[k]:
            if l.split("/")[-1] == fname:
                return l

    return None

# Get a list of all the root files possible to identify as required.
def extract_root_filenames(workflows, verbose = False, fname = None, fullPath = False):
    if(fname is not None):
        output_file = open(fname, "w")

    all_files = set()
    file_list = []
    for w in workflows:
        if len(w[1]) > 0:
            for root in w[1]:
                if root not in all_files:
                    all_files.add(root)
                    if fullPath:
                        a_path = restore_path(root)
                    else:
                        a_path = root.split('/')[-1]
                    if verbose:
                        print a_path
                        print w[2]
                    file_list.append([a_path, w[2]])
                    if(fname is not None):
                        print 'saving to file: ' + fname
                        output_file.write(a_path + '\n' + str(w[2]) + '\n')
    return file_list
 
# Save the current workflow table if doesn't already exists.
def persist_table(workflow):
    if not os.path.isfile('/workflows'):
        pickle.dump(workflow, open('/workflows', 'w'))

# Get output file name, used in --manual_check
def extract_output(command):
    try:
        ret = command.split('>')[1].split(' ')[1]
    except IndexError:
        ret = None
    return ret

# Alpha function TODO: Remove this? Not used function.
def extract_command_files(command):
    if 'das_client.py' in command:
        print 'das_client command'
        return []

    elif '--filein' in command:
        print 'Using das_client.py output'
        return []

    elif extract_output(command) is None:
        print 'Empty log'
        return []

    else:
        print 'Running'
        print command
        print extract_output(command)
        p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        p.wait()

# Remove --no_exec flag, used in --manual_check
def remove_no_exec_flag(command):
    return command.replace("--no_exec", "")

# Get cut number specifid in a step
def extract_cut_number(new_step):
    cut = 0
    new_step_split = new_step.split(' ')
    cut_il = [ind for ind in range(len(new_step_split)) if "-n" == new_step_split[ind]]
    cut = max([cut] + ([int(new_step_split[j+1]) for j in cut_il]))
    return cut

# Transfer remote file using sshpass (quicker) and cut it.
def getRemoteAndCut(login, password, remote, filepath, filename, cut):
    fix_pass = password.replace('`', '\\`').replace('(', '\\(')
    comm = 'sshpass -p ' + fix_pass + " scp -r " + login + "@" + remote.split(":")[0] + ":" + filepath + " " + "/data/" + filename
    print comm
    p = subprocess.Popen(comm, shell=True, stdout=subprocess.PIPE)
    p.wait()
    cut_root_file(filename, cut)

# Gracefully print a table.
def pretty_print_table(names, table, spacing):
    maxsize = []

    for title in names:
        maxsize.append(len(title)+spacing)

    for i in range(len(names)):
        for line in table:
            aux = (len(str(line[i]))+spacing)
            if(  aux > maxsize[i] ):
                maxsize[i] = aux

    for index in range(len(names)):
        sys.stdout.write(str(names[index]).ljust(maxsize[index]))

    for line in table:
        sys.stdout.write('\n')
        for index in range(len(names)):
            sys.stdout.write(str(line[index]).ljust(maxsize[index]))
    sys.stdout.write('\n')

# ArgParse
parser = argparse.ArgumentParser(description='Tool for parsing and generating tools for Offline CMS-Docker.')

subparsers = parser.add_subparsers(help='sub-command help', dest='command')

parser_creating = subparsers.add_parser('creating', help='Parse the creating.log and get info from it.')
parser_creating.add_argument('--steps', action="store_true", dest="steps", help="Get only steps")
parser_creating.add_argument('--files', action="store_true", dest="files", help="Get only filenames")
parser_creating.add_argument('--output', action="store", dest="output", help="Save output on file")

parser_truncate = subparsers.add_parser('truncate', help='Truncate the root files and save them locally')
parser_truncate.add_argument('--remote', action="store_true", dest="remote", help="Root files are in remote, use Paramiko")
parser_truncate.add_argument('--path', action="store", dest="path", required=True, help="Path of the root files")
parser_truncate.add_argument('--remote_login', action="store", dest="remote_login", help="Login used for remote")
parser_truncate.add_argument('--remote_password', action="store", dest="remote_password", help="Password used for remote")
parser_truncate.add_argument('--manual_check', action="store_true", dest="manual_check", help="Manually tries to run workflows using creating.log and to fix missing files.")
parser_truncate.add_argument('--parse_logs', action="store_true", dest="parse_logs", help="Parse runTheMatrix logs checking for files.")

parser_matrix = subparsers.add_parser('matrix', help='Parser runTheMatrix output.')
parser_matrix.add_argument('--file', action="store", dest="file", help="File with the stdout from runTheMatrix.")

parser_das_recovery = subparsers.add_parser('das_recovery', help='Recovery the full path outputed by das_client.py.')
parser_das_recovery.add_argument('--file', action="store", dest="file", help="File a list of files to recovery the name.")

parser_docker = subparsers.add_parser('docker', help='Dockerize some workflows.')
parser_docker_argument('-i all', action="store_true", dest="include", help="If should use the flag -i all.")
parser_docker_argument('-l', action="store", dest="workflows", help="Selected workflows.")
parser_docker_argument('-time', action="store_true", dest="time", help="Time the runTheMatrix.py call.")

parsed = parser.parse_args()

# Necessary check for all of them. (? Not anymore)

# Check path for current folder creating.log.
if(os.getcwd() == '/'):
    print 'Error: you shouldn\'t run this at \\, what if your machine explodes?'
    sys.exit()

c_fname = os.getcwd() + "/creating.log"
dic_fname = "/das_dic"

# check if creating.log and das_dic files exist.
if not os.path.isfile(c_fname):
    print 'Error: creating.log file not found, use ./generate.sh.'
    sys.exit()

if not os.path.isfile(dic_fname):
    print 'Error: could not find /das_dic file. You should use custom das_client.py file.'
    sys.exit()

# Truncate option, will do everything it can to find and truncate the required files.
if parsed.command == 'truncate':
    # check if cms is on the path.
    if (which("cmsRun") is None):
        print 'Error:  Could not find cmsRun in path, use ./generate.sh or put cmsRun in path'
        sys.exit()

    # ArgParse verifications
    if parsed.remote and ((not parsed.remote_login) or (not parsed.remote_password)):
        print "Remote access requires login and password."
        sys.exit()

    print 'Trying to truncate root files'

    if parsed.remote:
        # Connect to remote host and get a list of all files available.
        import paramiko
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        address = parsed.path.split(":")[0]
        path = parsed.path.split(":")[1]
        print 'Trying to connect to ' + address
        ssh.connect(address, username=parsed.remote_login, password=parsed.remote_password)
        stdin, stdout, stderr = ssh.exec_command("find " + path + " -name \"*.root\"")

        # format [filename, remote path]
        remotes = stdout.readlines()
        for i in range(len(remotes)):
            remotes[i] = [remotes[i].split('/')[-1][:-1], remotes[i][:-1]]

        print 'Found ' + str(len(remotes)) + ' files.'

    else:
        print 'To be implemented.'
        sys.exit()

    
    print 'Parsing creating.log to get root filenames'
    workflows = parse_creating_log()
    root_names = extract_root_filenames(workflows)
    persist_table(workflows)

    hit = 0
    das_cut = []
    for rem in remotes:
        found = [x for x in root_names if x[0] == rem[0]]
        print found
        if len(found) > 0:
            found = found[0]
            found.append(rem[1])
            hit = hit+1
            print found[0]
            if not os.path.isfile('/data/' + found[0]):
                das_cut.append(found)
            else:
                print 'File : ' + '/data/ ' + found[0] + ' already here.'
        else:
            print 'Not found: ' + rem[0]
    print 'Das Required files : ' + str(hit) + '/' + str(len(remotes))
    print 'Missing: ' + str(len(das_cut)) + '/' + str(hit)

    # Getting remote files required by das
    if parsed.remote:
        if len(das_cut) > 1:
            print 'Starting to get and get files required from das_client.py'
        for new_file in das_cut:
            print 'Trying to truncate: ' + str(new_file)
            getRemoteAndCut(parsed.remote_login, parsed.remote_password, parsed.path, new_file[2], new_file[0], new_file[1])

            # Previous code.
            #fix_pass = parsed.remote_password.replace('`', '\\`').replace('(', '\\(')
            #comm = 'sshpass -p ' + fix_pass + " scp -r " + parsed.remote_login + "@" + parsed.path.split(":")[0] + ":" + new_file[2] + " " + "/data/" + new_file[0] 
            #print comm
            #p = subprocess.Popen(comm, shell=True, stdout=subprocess.PIPE)
            #p.wait()
            #cut_root_file(new_file[0], new_file[1])

    else:
        print 'Not implemented yet.'
        sys.exit()

    # At this point, every file required by DAS_client should be in /data/ and trucanted. 

    # Now, try to run the steps and check if files are opened correctly or not.
    if parsed.manual_check:
        w_c = 0
        corrected = 0
        problems = 0
        not_found_list = []
        for flow in workflows[30:]:
            print 'Starting #' + str(w_c) + ' : ' + flow[0]
            w_c = w_c + 1
            step_n = 1
            for step in flow[3]:
                # Try until success.
                pend = True
                while pend:
                    pend = False                

                    # Extract log_file name and remove 'no_exec' flag
                    log_file = extract_output(step)
                    no_exec_command = remove_no_exec_flag(step)
                    print 'Starting step : ' + str(step_n)
                    print no_exec_command

                    # Execute step
                    p = subprocess.Popen(remove_no_exec_flag(step), shell=True, stdout=subprocess.PIPE)
                    p.wait()
                    step_n = step_n+1

                    # Open log file of step
                    with open(log_file, 'r') as l_h:
                        log_lines = l_h.readlines()

                    # Search for files requested 
                    print 'Log file name: ' + log_file
                    required_files = []
                    for li in log_lines:
                        if 'Initiating request to open file' in li:
                            required_files.append(li.split('Initiating request to open file')[1])

                    # Try to find each of the files opened.
                    for req in required_files:
                        req_file = req.split('ile:')[1][:-1]
                        #print 'Looking for ' + req_file
                        # If couldn't find the file, try to get remotely.
                        if not os.path.isfile(req_file):
                            print ">>> FILE MISSING ERROR <<<"
                            print "ERROR: File not found : " + req.split('ile:')[1] 
                            filename = req_file.split('/')[-1]
                            found = [x for x in remotes if x[0] == filename]

                            if len(found) > 0:
                                getRemoteAndCut(parsed.remote_login, parsed.remote_password, parsed.path, found[0][1], found[0][0], extract_cut_number(step))

                                # Previous code, new function to remove this.
                                #print 'Found file in remote path, trying to solve.'
                                #fix_pass = parsed.remote_password.replace('`', '\\`').replace('(', '\\(')
                                #comm = 'sshpass -p ' + fix_pass + " scp -r " + parsed.remote_login + "@" + parsed.path.split(":")[0] + ":" + found[0][1] + " " + "/data/" + found[0][0] 
                                #print comm
                                #p = subprocess.Popen(comm, shell=True, stdout=subprocess.PIPE)
                                #p.wait()
                                #cut_root_file(found[0][1], extract_cut_number(step))

                                corrected = corrected + 1
                                pend = True
                            else:
                                print 'File (' + filename + ') does not exist in remote path. What now?'
                                problems = problems + 1
                                not_found_list.append(filename)

        # Print recovery information.
        print 'Count : ' + str(w_c)
        print 'Corrected : ' + str(corrected)
        print 'Problems : ' + str(problems)
        print 'List : ' + str(not_found_list)

    # Time to parse local runTheMatrix logs.
    if parsed.parse_logs:
        print ''
        print 'Parsing runTheMatrix log files.'
        dir = os.walk('.').next()[1] 

        # Find all runTheMatrix folders.
        matrixDirs = []
        for d in dir:
            if d[0].isdigit():
                matrixDirs.append(d)

        # Find all runTheMatrix logfile names.
        logFiles = []
        for d in matrixDirs:
            #print os.getcwd()+'/'+d
            arq = os.listdir(os.getcwd()+'/'+d)
            for f in arq:
                if '.log' in f:
                    logFiles.append([os.getcwd()+'/'+d+'/'+f, os.getcwd()+'/'+d + '/'])
        print 'Log files founded: ' + str(len(logFiles))

        # Find all requested files in the logfiles.
        requestedFiles = [] # format [filename, filepath, folderpath] (why all that?)
        for lf in logFiles:
            with open(lf[0]) as fi:
                logT = fi.readlines()
            for l in logT:
                if "Initiating request to open file file:" in l:
                    #if 'Initiating request to open file file:step1.root' in l:
                    #    print l.split('Initiating request to open file file:')[1][:-1]
                    #    print ('/data/' in l)
                    #    print lf[1] + l.split('Initiating request to open file file:')[1][:-1] 
                    if '/data/' in l:
                        requestedFiles.append([l.split('Initiating request to open file file:')[1][:-1], lf[1], lf[0]])
                    else:
                        requestedFiles.append([lf[1] + l.split('Initiating request to open file file:')[1][:-1], lf[1], lf[0]])
        print 'Requested files: ' + str(len(requestedFiles))         

        # Check which file is still missing and try to get them from remote.
        found_locally = 0
        prob = 0
        correct = 0
        still_missing = []
        for fn in requestedFiles:
            if not os.path.isfile(fn[0]):
                print 'Error: file not found: ' + fn[0] + ' in ' + fn[2]
                prob = prob+1
                filename = fn[0].split('/')[-1]
                found = [x for x in remotes if x[0] == filename]

                if len(found) > 0:
                    print fn[1]
                    with open(fn[1]+'/'+'cmdLog') as fc:
                        cm = fc.readlines()
                    for cl in cm:
                        #print fn[2].split('/')[-1]
                        if fn[2].split('/')[-1] in cl:
                            #print cl
                            cut_num = extract_cut_number(cl)

                    print 'Found file in remote, copying it to /data/'
                    getRemoteAndCut(parsed.remote_login, parsed.remote_password, parsed.path, found[0][1], found[0][0], cut_num)
                    correct = correct+1
                else:
                    still_missing.append(filename)
            else:
                found_locally = found_locally+1

        print 'Found : ' + str(found_locally) + "/" + str(len(requestedFiles))
        print 'Correct : ' + str(correct) + "/" + str(len(requestedFiles)-len(found))
        print 'Missing : ' + str(len(still_missing))
        for miss in still_missing:
            print miss

# Creating option, will just parse files and generate outputs.
elif parsed.command == 'creating':
    print "Parsing creating.log file"
    fname = parsed.output
    if(parsed.output is not None):
        try:
            os.remove(fname)
        except OSError:
            pass

    #cut_root_file('221E066C-69D1-E311-8E63-0026189438FA.root', 100)
    workflows = parse_creating_log()

    if not parsed.files:
        if(fname is not None):
            output_file =open(fname, "w")

    if not parsed.files:
        for w in workflows:
            print "--- NEW WORKFLOW ---" 
            print w[0]
            if(fname is not None):
                output_file.write("--- NEW WORKFLOW ---\n")
                output_file.write(str(w[0]) + '\n')

            for step in w[3]:
                print step
                if(fname is not None):
                    output_file.write(step + '\n')

            if not(parsed.steps):
                if len(w[1]) > 0:
                    print "--- Files ---"
                    if(fname is not None):
                        output_file.write("--- Files ---\n")
                    for a_file in w[1]:
                        print a_file
                        if(fname is not None):
                            output_file.write(afile + "\n")
    else: 
        extract_root_filenames(workflows, True, fname, True)

# Parse the stdout of runTheMatrix and output a clean table to see which of the workflows are working or not.
elif parsed.command == 'matrix':
    
    if not parsed.file:
        print 'You have to specify a file with stdout from runTheMatrix.py'
        sys.exit()

    if not os.path.isfile(parsed.file):
        print 'Could not find runTheMatrix stdout file (' + parsed.file + ')'
        sys.exit()

    with open(parsed.file) as fi:
        rtm_l = fi.readlines()

    # Find the last 'x'
    l_x = 0 
    for i in range(len(rtm_l)):
        if(rtm_l[i] == 'x\n'):
            l_x = i

    # Print variables.
    rtm_l = rtm_l[l_x+1:]
    table = []
    names = ['Workflow', 'Step0', 'Step1', 'Step2', 'Step3', 'Step4', 'Step5', 'Status']
    success = 0
    valid = 0
     
    # Go through each line after the l_x+1th.
    for l in rtm_l:
        l_name = l.split('Step')[0][:8]
        l_s = l.split('Step')[1:]
        # if can find steps.
        if len(l_s) > 0:
            valid = valid+1
            # clean last step status
            l_s[-1] = l_s[-1].split(' ')[0]
            elem = [l_name]
            # Append each step and remove any eventual space
            for s in l_s:
                elem.append(s[2:].replace(' ', ''))
            # Fill table when it overs before step5.
            while len(elem) < 7:
                elem.append("---")
            # Check if everyone has passed.
            passed = 'YES'
            for i in range(1, len(elem)): 
                if (elem[i] != "PASSED") and (elem[i] != "---"):
                    passed = 'NO'
            if(passed == 'YES'):
                success = success+1
            # Append into table.
            elem.append(passed) 
            table.append(elem)
            #print elem
            
    # Print the table of workflows with status for each step.
    pretty_print_table(names, table, 2)
    print "Workflows: " + str(valid)
    print "Success:  " + str(success)

# Recover full path from das_dic.
elif parsed.command == 'das_recovery':
    if not parsed.file:
        print 'You have to specify a file with a list of required files.'
        sys.exit()

    if not os.path.isfile(parsed.file):
        print 'Could not find the list of files (' + parsed.file + ')'
        sys.exit()

    with open(parsed.file) as fi:
        file_list = fi.readlines()
    
    repeat_count = 0 
    das_dic = pickle.load(open('/das_dic', 'rb'))
    match = {}
    # For each file in the list.
    for fil in file_list:
        fname = fil[:-1]
        # Check if there is a repeated entry in the file list.
        if fname in match:
            print 'Repeated entry : ' + fname
            repeat_count = repeat_count + 1
        else:
            # Check for all das_dic entries.
            for key, value in das_dic.iteritems():
                # Check in each file output from current das result.
                for lin in value:
                    # Search for the name in the path.
                    if fname in lin:
                        match[fname] = lin
            if fname not in match:
                print "Can't find " + fname

    # Output success rate.
    print "Total files found: " + str(len(match) + repeat_count) + "/" + str(len(file_list))

    # And complete path recovered.
    for key, value in match.iteritems():
        print value

elif parsed.command == 'docker':
    cmd = os.getcwd() + '/care-x86_64'
    if parsed.time:
        cmd = cmd + ' time'

    cmd = cmd + ' runTheMatrix.py'

    if parsed.include:
        cmd = cmd + ' -i all'

    if parsed.workflows:
        cmd = cmd + ' -l' + parsed.workflows

    print cmd
