#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <dlfcn.h>
#include "uthash.h"
#include "openssl/md5.h"
#include "openssl/sha.h"

typedef unsigned long FrontierChannel;
typedef void FrontierRSBlob;

struct offline_entry {
    char * query;
    char * data;
    UT_hash_handle hh;
};


// Channel Structure

#define FRONTIER_MAX_PAYLOADNUM	32
#define FRONTIER_MAX_PROXYN	24
#define FRONTIER_MAX_SERVERN	16
#define FRONTIER_HTTP_BUF_SIZE	(32*1024)
#define FRONTIER_HTTP_DEBUG_BUF_SIZE 512
#define FRONTIER_MAX_PROXYCONFIGN 8

#define hashtable s_fn_hashtable

struct entry
{
    void *k, *v;
    unsigned int h;
    struct entry *next;
};

struct hashtable {
    unsigned int tablelength;
    struct entry **table;
    unsigned int entrycount;
    unsigned int loadlimit;
    unsigned int primeindex;
    unsigned int (*hashfn) (void *k);
    int (*eqfn) (void *k1, void *k2);
};

struct s_fn_client_cache_list {
    struct s_fn_client_cache_list *next;
    struct s_fn_hashtable *table;
    char *servlet;
};
typedef struct s_fn_client_cache_list fn_client_cache_list;

struct s_FrontierConfig
 {
  char *server[FRONTIER_MAX_SERVERN];
  char *proxy[FRONTIER_MAX_PROXYN];
  char *proxyconfig[FRONTIER_MAX_PROXYCONFIGN];
  int server_num;
  int proxy_num;
  int proxyconfig_num;
  int server_cur;
  int proxy_cur;
  int proxyconfig_cur;
  int servers_balanced;
  int proxies_balanced;
  int num_backupproxies;
  int connect_timeout_secs;
  int read_timeout_secs;
  int write_timeout_secs;
  int max_age_secs;
  char *force_reload;
  char *freshkey;
  int retrieve_zip_level;
  int secured;
  char *capath;
  int client_cache_max_result_size;
  int failover_to_server;
 };
typedef struct s_FrontierConfig FrontierConfig;

struct s_FrontierMemBuf
 {
  struct s_FrontierMemBuf *nextbuf;
  size_t len;
 };
typedef struct s_FrontierMemBuf FrontierMemBuf;

struct s_fn_b64a2b_context {
    int dlast;
    int phase;
};
typedef struct s_fn_b64a2b_context fn_b64a2b_context;

struct s_FrontierMemData
 {
  FrontierMemBuf *firstbuf;
  FrontierMemBuf *lastbuf;
  size_t total;
  size_t zipped_total;
  int error;
  fn_b64a2b_context b64context;
  unsigned char md5[MD5_DIGEST_LENGTH];
  MD5_CTX md5_ctx;
  unsigned char sha256[SHA256_DIGEST_LENGTH];
  SHA256_CTX sha256_ctx;
  int secured;
  int binzipped;
  unsigned char zipbuf[4096];
  int zipbuflen;
 };
typedef struct s_FrontierMemData FrontierMemData;

struct s_FrontierPayload
 {
  int id;
  char *encoding;
  int error;
  int error_code;
  char *error_msg;
  unsigned char *blob;
  int blob_size;
  unsigned int nrec;
  long full_size;
  char srv_md5_str[MD5_DIGEST_LENGTH*2+1];
  unsigned char digest[SHA256_DIGEST_LENGTH];
  unsigned char *srv_sig;
  int srv_sig_len;
  FrontierMemData *md;
 };
typedef struct s_FrontierPayload FrontierPayload;

struct s_FrontierResponse
 {
  int error;
  int payload_num;
  int error_payload_ind;
  int keepalives;
  int max_age;
  void *parser;
  int p_state;
  int zipped;
  int seqnum;
  void *srv_rsakey;
  const char *params1;
  const char *params2;
  FrontierPayload *payload[FRONTIER_MAX_PAYLOADNUM];
 };
typedef struct s_FrontierResponse FrontierResponse;

struct s_FrontierAddrInfo
 {
  struct s_FrontierAddrInfo *next;
  struct addrinfo *addr;
  int haderror;
 };
typedef struct s_FrontierAddrInfo FrontierAddrInfo;

struct s_FrontierUrlInfo
 {
  char *url;
  char *proto;
  char *host;
  int port;
  char *path;
  FrontierAddrInfo firstfai;
  FrontierAddrInfo *fai;
  FrontierAddrInfo *lastfai;
 };
typedef struct s_FrontierUrlInfo FrontierUrlInfo;

struct s_FrontierHttpClnt
 {
  FrontierUrlInfo *proxy[FRONTIER_MAX_PROXYN];
  FrontierUrlInfo *server[FRONTIER_MAX_SERVERN];
  int cur_proxy;
  int cur_server;
  int total_proxy;
  int total_server;
  int balance_num_proxies;
  int balance_servers;
  int first_proxy;
  int first_server;
  unsigned rand_seed;
  int refresh_flag;
  int max_age;
  int age;
  int using_proxy;
  int content_length;
  int total_length;
  char *url_suffix;
  char *frontier_id;
  time_t whenresetproxy;
  time_t whenresetserver;

  int socket;
  int connect_timeout_secs;
  int read_timeout_secs;
  int write_timeout_secs;
  struct addrinfo *cur_addr;

  int err_code;
  int data_size;
  int data_pos;
  char buf[FRONTIER_HTTP_BUF_SIZE];
  char proxybuf[FRONTIER_HTTP_DEBUG_BUF_SIZE];
  char serverbuf[FRONTIER_HTTP_DEBUG_BUF_SIZE];
 };
typedef struct s_FrontierHttpClnt FrontierHttpClnt;

struct s_Channel
 {
  FrontierConfig *cfg;
  FrontierResponse *resp;
  FrontierHttpClnt *ht_clnt;
  pid_t pid;
  int http_resp_code;
  int refresh;  // Refresh requested (1=soft, 2=hard)
  int max_age;  // Max cache age for soft refresh
  int ttl;      // Time-to-live requested by API (1=short, 2=long, 3=forever)
  int seqnum;      // sequence number for the channel
  int response_seqnum; // next sequence number for responses using this channel
  fn_client_cache_list *client_cache;
  char *client_cache_buf;
  char *ttlshort_suffix;
  char *ttllong_suffix;
  char *ttlforever_suffix;
  int client_cache_maxsize;
  void *serverrsakey[FRONTIER_MAX_SERVERN];
 };
typedef struct s_Channel Channel;

// End of Channel

struct offline_entry * dic = NULL;

/*frontierRSBlob_get is deprecated, use frontierRSBlob_open instead*/
//FrontierRSBlob *frontierRSBlob_get(FrontierChannel u_channel,int n,int *ec);
//FrontierRSBlob *frontierRSBlob_open(FrontierChannel u_channel,FrontierRSBlob *oldrs,int n,int *ec);
//void frontierRSBlob_close(FrontierRSBlob *rs,int *ec);

//int frontier_init(void *(*f_mem_alloc)(size_t size),void (*f_mem_free)(void *ptr));

//void frontier_closeChannel(FrontierChannel chn);
//void frontier_setTimeToLive(FrontierChannel u_channel,int ttl); // 1=short, 2=long, 3=forever

//int frontier_getRawData(FrontierChannel chn,const char *uri);

static int (* original_frontier_postRawData)(FrontierChannel chn,const char *uri,const char *body) = NULL; 
static FrontierChannel (* original_frontier_createChannel)(const char *srv,const char *proxy,int *ec) = NULL;
void (* original_frontier_setTimeToLive) (FrontierChannel u_channel,int ttl) = NULL;

void frontier_setTimeToLive(FrontierChannel u_channel,int ttl) {

    if (original_frontier_setTimeToLive == NULL) {
        original_frontier_setTimeToLive = dlsym(RTLD_NEXT, "frontier_setTimeToLive");
    }

    original_frontier_setTimeToLive(u_channel, ttl);
}

int frontier_postRawData(FrontierChannel chn,const char *uri,const char *body) {
    Channel * chn_cast=(Channel*)chn;
    int ret;

    printf("frontier_postRawData(FrontierChannel = %lu, uri = %s, body = %s)\n", chn, uri, body);

    if (original_frontier_postRawData == NULL) {
        original_frontier_postRawData = dlsym(RTLD_NEXT, "frontier_postRawData");
    }

    ret = original_frontier_postRawData(chn, uri, body);
    printf("Returning from frontier_postRawData(FrontierChannel = %lu, uri = %s, body = %s)\n", chn, uri, body);
    return ret;
}

FrontierChannel frontier_createChannel(const char *srv,const char *proxy,int *ec) {
    //printf("frontier_createChannel(srv = %s, proxy = %s, *ec = %d)\n", srv, proxy, *ec);
    //printf("frontier_createChannel call.\n");

    if (original_frontier_createChannel == NULL) {
        original_frontier_createChannel = dlsym(RTLD_NEXT, "frontier_createChannel");
    }
    //printf("%x\n", original_frontier_createChannel);

    FrontierChannel ret = original_frontier_createChannel(srv, proxy, ec);
    //printf("returned\n");
    //printf("new pointer: %x\n", original_frontier_createChannel);
    //printf("%x\n", ret);

    return ret;
    //return ((int(*)(const char*, const char*, int*))dlsym(RTLD_NEXT, "frontier_createChannel"))(srv, proxy, ec);

}

int main () {
    struct offline_entry * x, * y;

    x = (struct offline_entry *) malloc (sizeof(struct entry));
    x->query = (char *) malloc (sizeof(char)*2);
    x->query[0] = 'a'; x->query[1] = '\0';

    x->data = (char *) malloc (10);
    x->data[0] = 'b';

    HASH_ADD_STR(dic, query, x); 
    HASH_FIND_STR(dic, "a", y);
    if(y) printf("Found A: %s\n", y->data);
}
