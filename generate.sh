#!/bin/bash

# check for creating.log filename
current=$(pwd)

# check and upgrade modified scripts if needed.
for dir in /opt/cms/*/
do
    find $dir -name "das_client.py" | while read f
    do
        diff=$(diff CMS_scripts/das_client.py $f)
        if [ -n "$diff" ];
        then
            echo Updating $f 
            cp -f CMS_scripts/das_client.py $f
        fi
    done

    find $dir -name "WorkFlowRunner.py" | while read f
    do
        diff=$(diff CMS_scripts/WorkFlowRunner.py $f)
        if [ -n "$diff" ];
        then
            echo Updating $f 
            cp -f CMS_scripts/WorkFlowRunner.py $f
        fi
    done
done

# check if not /, should not run there.
if [ "$current" == "/" ];
then
    echo "Please, find a better place to run this script."
    kill -INT $$
fi

# check if /run.sh exists and, if it does, run it.
if [ -f /run.sh ];
then
    echo "Activating run.sh."
else
    echo "Could not find /run.sh, please add it."
    kill -INT $$
fi

source /run.sh
cd $current

# Check for creating.log, if not available, enters the Matrix to generate it.
filename="/creating.log"
creating=$current$filename
echo Looking for $creating.
if [ -f $creating ];
then
    echo "$FILE exists."
else
    echo "$FILE does not exist, generating it."

    # runTheMatrix to generate creating.log
    mkdir -p tmp
    cd tmp
    runTheMatrix.py -i all --what standard --wmcontrol init --noCafVeto | tee creating.log

    #remove unnecessary files
    cp creating.log ../
    cd ..
    rm -rf tmp/
fi

# At this point das_dic file should be there. If not, something is wrong, should try again.
das_dic="/das_dic"
if [ ! -f $das_dic ];
then
    echo "Could not find the /das_dic file. Remove creating.log and try again."
    kill -INT $$
fi

# At this point we have creating.log and we could run using generateWorkflows.py
filename="/generateWorkflows.py"
generatepy=$current$filename
echo $generatepy
if [ -f $generatepy ];
then
    python generateWorkflows.py creating
else
    echo "Could not find generateWorkflows.py in the current folder, be kind and put it here."
    kill -INT $$
fi
