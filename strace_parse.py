# strace_parse.py : small script to retrieve open files list from strace dumps.

import sys
import re
import argparse

# set parser 
parser = argparse.ArgumentParser(description='Select open syscalls from strace dump.')
parser.add_argument('filen', metavar='filename', type=str, nargs=1,
                   help='Name of the strace dump')
parser.add_argument('--r', action='store_true',
                   help='Also print the type and result of the open syscall.')
parser.add_argument('--s', action='store_true',
                   help='Save the result in a file $filename.open')

args = parser.parse_args()

# check arguments
fname = str(args.filen[0])
p_res = args.r
should_save = args.s

# try to open and read the file.
try:
    with open(fname) as f:
        content = f.readlines()
except IOError:
    print "File " + fname + " not found."
    sys.exit()

# select open syscalls
out = []
for entry in content:
    if entry.startswith("open"):
        split = re.split(',|\(|\n|=|\ ', entry)
        if(p_res):
            out.append(split[1][1:-1] + " -> " + str(split[3]) + " -> " + str(split[6]))
        else:
            out.append(split[1][1:-1])

if should_save:
    # save results in new file.
    with open(str(fname) + ".open", 'w') as file:
        for item in out:
            file.write(item + '\n')
else:
    for item in out:
            print item
