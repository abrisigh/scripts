import os.path
import os
import sys
import subprocess

# Cut a root file and save it in the correct position. Checks if it locally and, if it is, replace it.
def cut_root_file(filename, events):
    print 'Cutting ' + filename + ' to ' + str(events) +  ' events.'    
    try:
        os.remove("cut.py")
    except OSError:
        pass

    with open("cut.py", "w") as cut:
        # Give the process a name
        cut.write("process = cms.Process(\"PickEvent\")\n")

        # Tell the process which files to use as the sourdce
        cut.write("process.source = cms.Source (\"PoolSource\",\n")
        cut.write("            fileNames = cms.untracked.vstring (\"" + filename + "\")\n")
        cut.write(")\n")

        # tell the process to only run over 100 events (-1 would mean run over
        #  everything
        cut.write("process.maxEvents = cms.untracked.PSet(\n")
        cut.write("            input = cms.untracked.int32 ("+ str(events) + ")\n")
        cut.write(")\n")

        # Tell the process what filename to use to save the output
        cut.write("process.Out = cms.OutputModule(\"PoolOutputModule\",\n")
        cut.write("         fileName = cms.untracked.string (\"" + filename.split("/")[-1]  + "\")\n")
        cut.write(")\n")

        # make sure everything is hooked up
        cut.write("process.end = cms.EndPath(process.Out)\n")

    cmsRun = subprocess.Popen('cmsRun cut.py', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cmsRun.wait()
    print "Cutting" + filename + " finished" 

# Used to check if CMSRun exists.
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None


# Check cmsRun and / execution
if (which("cmsRun") is None):
    print 'Error:  Could not find cmsRun in path'
    sys.exit()

if(os.getcwd() == '/'):
    print 'Error: you shouldn\'t run this at \\, what if your machine explodes?'
    sys.exit()

# Get everyone from the file
with open("allfiles", "r") as all:
    lines = all.readlines()

for i in range(len(lines)/2):
    #print lines[i*2][:-1]      # filename
    #print lines[i*2+1][:-1]    # number of events
    cut_root_file(lines[i*2][:-1], lines[i*2+1][:-1])

